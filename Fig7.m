clear all; close all; clc

% Alter figure label and text font sizes
set(0,'defaultaxesfontsize',15);
set(0,'defaulttextfontsize',15);

nVals = [8 16 32 64];
clrMat = [[0 0 0]; [.3 .3 .3]; [.5 .5 .5]; [.8 .8 .8]];

% Jump response plotted on a fine grid
fineGrid = linspace(-pi,pi, 512).';

% The true jump function
trueJmpFnc = (fineGrid==0);

err = zeros( length(fineGrid), length(nVals) );

% for idx = 1:length(nVals)
% 
%     N = nVals(idx);             % No. of modes to plot is 2N+1
%     fourModes = (-N:N).';       % The Fourier modes
% 
%     % Physical grid
%     nPts = 256;
%     h = 2*pi/nPts;
%     phyGrid = -pi + h*(0:nPts-1).';
%     
%     jmpKern = exp( 1i*phyGrid*fourModes(fourModes~=0).' )/(2*pi);
%     fourKern = exp( 1i*fineGrid*fourModes(fourModes~=0).' )/(2*pi);
%     
%     % CVX problem
%     cvx_begin
%         variable sig(2*N+1);
%     
%         jmpRespCfs = sig(fourModes~=0)./abs( fourModes(fourModes~=0) );
%         jmpResp = real( jmpKern*jmpRespCfs );
%         minimize norm( jmpResp, 2 )
%             subject to
%                 jmpResp(phyGrid==0) == 1;
%     cvx_end
% 
%     err(:,idx) = log10( abs( trueJmpFnc - real( fourKern*jmpRespCfs ) ) );
%         
% end
% 
% locs1 = envelopDetect( err(:,1), 1.5e-2, -1 );
% locs2 = envelopDetect( err(:,2), 1.25e-2, -1.3 );
% locs3 = envelopDetect( err(:,3), .35e-1, -1.6 );
% locs4 = envelopDetect( err(:,4), 2e-1, -2 ); 
% [ err4 redLocs ] = localMax( err(locs4,4) );
% locs4(redLocs) = [];
% 
% figure(1); plot(fineGrid(locs1), err(locs1,1), 'k', 'linewidth', 2 );
% hold on; plot(fineGrid(locs2), err(locs2,2), 'color', [.3 .3 .3], 'linewidth', 2 );
% plot(fineGrid(locs3), err(locs3,3), 'color', [.5 .5 .5], 'linewidth', 2 );
% plot(fineGrid(locs4), err4, 'color', [.8 .8 .8], 'linewidth', 2 );
% xlabel x; ylabel 'Log absolute error'; xlim([-pi pi]); ylim([-4 0.25])
% legend('N=8', 'N=16', 'N=32', 'N=64' )


err = zeros( length(fineGrid), length(nVals) );

for idx = 1:length(nVals)

    N = nVals(idx);             % No. of modes to plot is 2N+1
    fourModes = (-N:N).';       % The Fourier modes

    % Physical grid
    nPts = 256;
    h = 2*pi/nPts;
    phyGrid = -pi + h*(0:nPts-1).';
    
    jmpKern = exp( 1i*phyGrid*fourModes(fourModes~=0).' )/(2*pi);
    fourKern = exp( 1i*fineGrid*fourModes(fourModes~=0).' )/(2*pi);
    
    % CVX problem
    cvx_begin
        variable sig(2*N+1);
    
        jmpRespCfs = sig(fourModes~=0)./abs( fourModes(fourModes~=0) );
        jmpResp = real( jmpKern*jmpRespCfs );
        minimize norm( jmpResp, 1 )
            subject to
                jmpResp(phyGrid==0) == 1;
    cvx_end

    err(:,idx) = log10( abs( trueJmpFnc - real( fourKern*jmpRespCfs ) ) );
        
end

locs1 = envelopDetect( err(:,1), 1.5e-2, -1.5 );
locs2 = envelopDetect( err(:,2), 1.25e-2, -2 );
locs3 = envelopDetect( err(:,3), .35e-1, -3 );
locs4 = envelopDetect( err(:,4), 2e-1, -4 ); 
[ err4 redLocs ] = localMax( err(locs4,4) );
locs4(redLocs) = [];
[ err4 redLocs ] = localMax( err4 );
locs4(redLocs) = [];

figure(2); plot(fineGrid(locs1), err(locs1,1), 'k', 'linewidth', 2 );
hold on; plot(fineGrid(locs2), err(locs2,2), 'color', [.3 .3 .3], 'linewidth', 2 );
plot(fineGrid(locs3), err(locs3,3), 'color', [.5 .5 .5], 'linewidth', 2 );
plot(fineGrid(locs4), err4, 'color', [.8 .8 .8], 'linewidth', 2 );
xlabel x; ylabel 'Log absolute error'; xlim([-pi pi]); ylim([-4 0.25])
legend('N=8', 'N=16', 'N=32', 'N=64' )
