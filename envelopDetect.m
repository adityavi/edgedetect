function locs = envelopDetect( inVec, thr, thrAlt )

len = length(inVec);
locs = (1:len).';
e = ones(len,1); z = zeros(len,1);
A = spdiags([e z -e], [-1 0 1], len, len);
locs( (abs(A*inVec)>thr) ) = [];
locs( inVec(locs)<thrAlt  ) = [];

return