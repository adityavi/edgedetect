clear all; close all; clc

% Alter figure label and text font sizes
set(0,'defaultaxesfontsize',15);
set(0,'defaulttextfontsize',15);


N = 64;                     % No. of modes to plot is 2N+1
fourModes = (-N:N).';       % The Fourier modes
k = [-N:-1 1 1:N].';

% Missing Modes
positiveModes = (1:N).';
missingModes = (( positiveModes>=30 ).*( positiveModes<=40 ) == 1);

% Physical grid
nPts = 256;
h = 2*pi/nPts;
phyGrid = -pi + h*(0:nPts-1).';

jmpKern = exp( 1i*phyGrid*fourModes.' )/(2*pi);

% CVX problem
cvx_begin
    variable sig(N);
    
    jmpRespCfs = [flipud(sig); 0; sig]./abs(k);
    jmpResp = real( jmpKern*jmpRespCfs );
        
    minimize norm( jmpResp, 1 )
        subject to
            jmpResp(phyGrid==0) == 1;
            sig( missingModes ) == 0;
            norm( jmpResp(abs(phyGrid)>=0.35), inf ) <= 1e-3;
cvx_end

figure; plot( fourModes, [flipud(sig); 0; sig], 'k--');
xlabel k; ylabel '\sigma(|k|/N)'; xlim([-80 80]); %ylim([0 4])

% The unit ramp
ramp = ( (-phyGrid-pi).*(phyGrid<=0) + (-phyGrid+pi).*(phyGrid>0) )/(2*pi);
figure; plot(phyGrid, ramp, 'k--'); hold on
plot( phyGrid, jmpResp, 'k');
xlabel x; ylabel 'W_0^\sigma^,^N(x)'; xlim([-pi pi]); ylim([-.75 1.25])
legend( 'f', 'S_N^\sigma[f]' );

sig = [flipud(sig); 0; sig];
save concFacProbMissing.mat sig;
