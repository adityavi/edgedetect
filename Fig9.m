clear all; close all; clc

% Alter figure label and text font sizes
set(0,'defaultaxesfontsize',15);
set(0,'defaulttextfontsize',15);


N = 64;                     % No. of modes to plot is 2N+1
fourModes = (-N:N).';       % The Fourier modes

% Physical grid
nPts = 256;
h = 2*pi/nPts;
phyGrid = -pi + h*(0:nPts-1).';

jmpKern = exp( 1i*phyGrid*fourModes(fourModes~=0).' )/(2*pi);


% CVX problem
cvx_begin
    variable sig(2*N+1);
    
    jmpRespCfs = sig(fourModes~=0)./abs( fourModes(fourModes~=0) );
    jmpResp = ( jmpKern*jmpRespCfs );
    
    kernW1Cfs = sig(fourModes~=0)./( abs( fourModes(fourModes~=0) ).*... 
        (1i*fourModes(fourModes~=0)) );
    kernW1 = ( jmpKern*kernW1Cfs );
    
    kernW2Cfs = sig(fourModes~=0)./( abs( fourModes(fourModes~=0) ).*... 
        (1i*fourModes(fourModes~=0)).^2 );
    kernW2 = ( jmpKern*kernW2Cfs );    
    
    minimize norm( jmpResp, 1 )
        subject to
            jmpResp(phyGrid==0) == 1;
            norm( kernW1, inf ) <= 1e-1;
            norm( kernW2, inf ) <= 1e-3;
            sig >= 0;
            sig(1) == 0; sig(end) <= 1e-2;
            
cvx_end

figure; plot( fourModes, sig, 'k--');
xlabel k; ylabel '\sigma(|k|/N)'; xlim([-80 80]); ylim([0 4])

% The unit ramp
ramp = ( (-phyGrid-pi).*(phyGrid<=0) + (-phyGrid+pi).*(phyGrid>0) )/(2*pi);

% Jump response plotted on a fine grid
nPts = 600;
h = 2*pi/nPts;
fineGrid = -pi + h*(0:nPts-1).';
jmpKern = exp( 1i*fineGrid*fourModes(fourModes~=0).' )/(2*pi);
jmpResp = real( jmpKern*jmpRespCfs );


figure; plot(phyGrid, ramp, 'k--'); hold on
plot( fineGrid, jmpResp, 'k');
xlabel x; ylabel 'W_0^\sigma^,^N(x)'; xlim([-pi pi]); ylim([-.75 1.25])
legend( 'f', 'S_N^\sigma[f]' );

save concFacProbForm3.mat sig
