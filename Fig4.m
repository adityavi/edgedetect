clear all; close all; clc

% Alter figure label and text font sizes
set(0,'defaultaxesfontsize',15);
set(0,'defaulttextfontsize',15);


N = 64;                     % No. of modes to plot is 2N+1
fourModes = (-N:N).';       % The Fourier modes

% Compute the coefficients of the jump responses
sigW0 = confac( fourModes, 'Poly' )./abs(fourModes);
sigW0(fourModes==0) = 0;

sigW1 = confac( fourModes, 'Poly' )./( abs(fourModes).*(1i*fourModes) );
sigW1(fourModes==0) = 0;

sigW2 = confac( fourModes, 'Poly' )./( abs(fourModes).*(1i*fourModes).^2 );
sigW2(fourModes==0) = 0;

sigW3 = confac( fourModes, 'Poly' )./( abs(fourModes).*(1i*fourModes).^3 );
sigW3(fourModes==0) = 0;

% Compute jump responses
% Physical grid
nPts = 1025;
h = 2*pi/nPts;
phyGrid = -pi + h*(0:nPts-1).';
fourKern = exp( i*phyGrid*fourModes.' )/(2*pi);

% The jump responses
kernW0 = real( fourKern*sigW0 );
kernW1 = real( fourKern*sigW1 );
kernW2 = real( fourKern*sigW2 );
kernW3 = real( fourKern*sigW3 );

% Plot
figure; plot(phyGrid, kernW0, 'k');
xlabel x; ylabel 'W_0^\sigma^,^N(x)'; xlim([-pi pi]);

figure; plot(phyGrid, kernW1, 'k');
xlabel x; ylabel 'W_1^\sigma^,^N(x)'; xlim([-pi pi]);

figure; plot(phyGrid, kernW2, 'k');
xlabel x; ylabel 'W_2^\sigma^,^N(x)'; xlim([-pi pi]);

figure; plot(phyGrid, kernW3, 'k');
xlabel x; ylabel 'W_3^\sigma^,^N(x)'; xlim([-pi pi]);
