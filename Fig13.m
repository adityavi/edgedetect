clear all; close all; clc

% Alter figure label and text font sizes
set(0,'defaultaxesfontsize',15);
set(0,'defaulttextfontsize',15);


N = 64;                     % No. of modes to plot is 2N+1
fourModes = (-N:N).';       % The Fourier modes

% Physical grid
nPts = 256;
h = 2*pi/nPts;
phyGrid = -pi + h*(0:nPts-1).';

missingModes = (( abs(fourModes)>=30 ).*( abs(fourModes)<=40 ) == 1);

sig = confac( fourModes, 'Poly' );
sig( missingModes) = 0;

jmpKern = exp( 1i*phyGrid*fourModes(fourModes~=0).' )/(2*pi);

jmpRespCfs = sig(fourModes~=0)./abs( fourModes(fourModes~=0) );
jmpResp = real( jmpKern*jmpRespCfs );

% The unit ramp
ramp = ( (-phyGrid-pi).*(phyGrid<=0) + (-phyGrid+pi).*(phyGrid>0) )/(2*pi);
figure; plot(phyGrid, ramp, 'k--'); hold on
plot( phyGrid, jmpResp, 'k');
xlabel x; ylabel 'S_N^\sigma[f](x)'; xlim([-pi pi]); ylim([-.75 1.25])
legend( 'f', 'S_N^\sigma[f]' );

load concFacProbForm2.mat

sig( missingModes) = 0;

jmpRespCfs = sig(fourModes~=0)./abs( fourModes(fourModes~=0) );
jmpResp = real( jmpKern*jmpRespCfs );

% The unit ramp
figure; plot(phyGrid, ramp, 'k--'); hold on
plot( phyGrid, jmpResp, 'k');
xlabel x; ylabel 'S_N^\sigma[f](x)'; xlim([-pi pi]); ylim([-.75 1.25])
legend( 'f', 'S_N^\sigma[f]' );