clear all; close all; clc

% Alter figure label and text font sizes
set(0,'defaultaxesfontsize',15);
set(0,'defaulttextfontsize',15);


N = 64;                     % No. of modes to plot is 2N+1
fourModes = (-N:N).';       % The Fourier modes

% Physical grid
nPts = 1025;
h = 2*pi/nPts;
phyGrid = -pi + h*(0:nPts-1).';

% Generate test function
[fHat fx] = genTestFnc(fourModes, 'Dennis', phyGrid);

% Compute the coefficients of the jump responses (Trigonometric, polynomial,
% exponential)
sigTrig = fHat.*( 1i*sign( fourModes ).*confac( fourModes, 'Trig' ) ); 

% Compute jump responses
fourKern = exp( i*phyGrid*fourModes.' );

% The jump responses
jmpRespTrig = real( fourKern*sigTrig );

% Plot
figure; plot(phyGrid, fx, 'k--'); hold on
plot( phyGrid, jmpRespTrig, 'k');
xlabel x; ylabel 'S_N[f](x)'
xlim([-pi pi]); ylim([-2 2])
legend( 'f', 'S_N[f] - Trig.', 'location', 'southwest' );




N = 100;                     % No. of modes to plot is 2N+1
fourModes = (-N:N).';       % The Fourier modes

% Generate test function
[fHat fx] = genTestFnc(fourModes, 'Variation', phyGrid);

% Compute the coefficients of the jump responses (Trigonometric, polynomial,
% exponential)
sigTrig = fHat.*( 1i*sign( fourModes ).*confac( fourModes, 'Trig' ) ); 
sigExp = fHat.*( 1i*sign( fourModes ).*confac( fourModes, 'Exp' ) ); 

% Compute jump responses
fourKern = exp( i*phyGrid*fourModes.' );

% The jump responses
jmpRespTrig = real( fourKern*sigTrig );
jmpRespExp = real( fourKern*sigExp );

% Plot
figure; plot(phyGrid, fx, 'k--'); hold on
plot( phyGrid, jmpRespTrig, 'k');
plot( phyGrid, jmpRespExp, 'color', [0.75 .75 .75]);
xlabel x; ylabel 'S_N[f](x)'
xlim([-pi pi]); ylim([-1.2 1.2])
legend( 'f', 'S_N[f] - Trig.', 'S_N[f] - Exp.' );
