clear all; close all; clc

% Alter figure label and text font sizes
set(0,'defaultaxesfontsize',15);
set(0,'defaulttextfontsize',15);


N = 32;                     % No. of modes to plot is 2N+1
fourModes = (-N:N).';       % The Fourier modes

% Compute the coefficients of the jump responses (Trigonometric, polynomial,
% exponential)
sigTrig = confac( fourModes, 'Trig' )./abs(fourModes); 
sigTrig(fourModes==0) = 0;
sigPoly = confac( fourModes, 'Poly' )./abs(fourModes);
sigPoly(fourModes==0) = 0;
sigExp = confac( fourModes, 'Exp' )./abs(fourModes);
sigExp(fourModes==0) = 0;

% Compute jump responses
% Physical grid
nPts = 512;
h = 2*pi/nPts;
phyGrid = -pi + h*(0:nPts-1).';
fourKern = exp( i*phyGrid*fourModes.' )/(2*pi);

% The jump responses
jmpRespTrig = real( fourKern*sigTrig );
jmpRespPoly = real( fourKern*sigPoly );
jmpRespExp = real( fourKern*sigExp );

% The unit ramp
ramp = ( (-phyGrid-pi).*(phyGrid<=0) + (-phyGrid+pi).*(phyGrid>0) )/(2*pi);

% Plot
figure; plot(phyGrid, ramp, 'k--'); hold on
plot( phyGrid, jmpRespTrig, 'k');
xlabel x; xlim([-pi pi]); ylim([-.6 1.2])
legend( 'f', 'S_N^\sigma[f]' );

figure; plot(phyGrid, ramp, 'k--'); hold on
plot( phyGrid, jmpRespPoly, 'k');
xlabel x; xlim([-pi pi]); ylim([-.6 1.2])
legend( 'f', 'S_N^\sigma[f]' );

figure; plot(phyGrid, ramp, 'k--'); hold on
plot( phyGrid, jmpRespExp, 'k');
xlabel x; xlim([-pi pi]); ylim([-.6 1.2])
legend( 'f', 'S_N^\sigma[f]' );
