clear all; close all; clc

% Alter figure label and text font sizes
% set(0,'defaultaxesfontsize',15);
% set(0,'defaulttextfontsize',15);


N = 64;                     % No. of modes to plot is 2N+1
fourModes = (-N:N).';       % The Fourier modes

% Compute the coefficients of the kernels
sigTrigW1 = confac( fourModes, 'Trig' )./( abs(fourModes).*(1i*fourModes) );
sigTrigW1(fourModes==0) = 0;

sigTrigW2 = confac( fourModes, 'Trig' )./( abs(fourModes).*(1i*fourModes).^2 );
sigTrigW2(fourModes==0) = 0;

sigTrigW3 = confac( fourModes, 'Trig' )./( abs(fourModes).*(1i*fourModes).^3 );
sigTrigW3(fourModes==0) = 0;

sigTrigW4 = confac( fourModes, 'Trig' )./( abs(fourModes).*(1i*fourModes).^4 );
sigTrigW4(fourModes==0) = 0;

% Compute jump responses
% Physical grid
nPts = 1024;
h = 2*pi/nPts;
phyGrid = -pi + h*(0:nPts-1).';
fourKern = exp( i*phyGrid*fourModes.' )/(2*pi);

% The jump responses
kernW1 = real( fourKern*sigTrigW1 );
kernW2 = real( fourKern*sigTrigW2 );
kernW3 = real( fourKern*sigTrigW3 );
kernW4 = real( fourKern*sigTrigW4 );

% Plot
figure; plot(phyGrid, kernW1, 'k', 'linewidth', 2 ); hold on
plot( phyGrid, kernW2, 'color', [.3 .3 .3], 'linewidth', 2 );
plot( phyGrid, kernW3, 'color', [.5 .5 .5], 'linewidth', 2 );
plot( phyGrid, kernW4, 'color', [.8 .8 .8], 'linewidth', 2 );
xlabel x; ylabel 'W_i^\sigma^,^N(x)'; xlim([-pi pi]); ylim([-.065 .065])
legend('W_1^\sigma^,^N', 'W_2^\sigma^,^N', 'W_3^\sigma^,^N', ...
    'W_4^\sigma^,^N', 'orientation', 'horizontal', 'location', 'southeast' )



% Compute the coefficients of the kernels
sigTrigW1 = confac( fourModes, 'Exp' )./( abs(fourModes).*(1i*fourModes) );
sigTrigW1(fourModes==0) = 0;

sigTrigW2 = confac( fourModes, 'Exp' )./( abs(fourModes).*(1i*fourModes).^2 );
sigTrigW2(fourModes==0) = 0;

sigTrigW3 = confac( fourModes, 'Exp' )./( abs(fourModes).*(1i*fourModes).^3 );
sigTrigW3(fourModes==0) = 0;

sigTrigW4 = confac( fourModes, 'Exp' )./( abs(fourModes).*(1i*fourModes).^4 );
sigTrigW4(fourModes==0) = 0;

% The jump responses
kernW1 = real( fourKern*sigTrigW1 );
kernW2 = real( fourKern*sigTrigW2 );
kernW3 = real( fourKern*sigTrigW3 );
kernW4 = real( fourKern*sigTrigW4 );

% Plot
figure; plot(phyGrid, log10(abs(kernW1)), 'k', 'linewidth', 2 ); hold on
plot( phyGrid, log10(abs(kernW2)), 'color', [.3 .3 .3], 'linewidth', 2 );
plot( phyGrid, log10(abs(kernW3)), 'color', [.5 .5 .5], 'linewidth', 2 );
plot( phyGrid, log10(abs(kernW4)), 'color', [.8 .8 .8], 'linewidth', 2 );
xlabel x; ylabel 'log_1_0 | W_i^\sigma^,^N(x) |'; xlim([-pi pi]); ylim([-9 -1])
legend('W_1^\sigma^,^N', 'W_2^\sigma^,^N', 'W_3^\sigma^,^N', ...
    'W_4^\sigma^,^N', 'orientation', 'horizontal', 'location', 'southeast' )