clear all; close all; clc

% Alter figure label and text font sizes
set(0,'defaultaxesfontsize',15);
set(0,'defaulttextfontsize',15);

load concFacProbForm2.mat;
sigProb2 = sig;
load concFacProbForm3.mat;
sigProb3 = sig;

N = 64;                     % No. of modes to plot is 2N+1
fourModes = (-N:N).';       % The Fourier modes

% Physical grid
nPts = 1025;
h = 2*pi/nPts;
phyGrid = -pi + h*(0:nPts-1).';

% Generate test function
[fHat fx] = genTestFnc(fourModes, 'Variation', phyGrid);

% Compute the coefficients of the jump responses (Trigonometric, polynomial,
% exponential)
sigTrig = fHat.*( 1i*sign( fourModes ).*sigProb2 ); 

% Compute jump responses
fourKern = exp( i*phyGrid*fourModes.' );

% The jump responses
jmpRespTrig = real( fourKern*sigTrig );

% Plot
figure; plot(phyGrid, fx, 'k--'); hold on
plot( phyGrid, jmpRespTrig, 'k');
xlabel x; ylabel 'S_N[f](x)'
xlim([-pi pi]); ylim([-1.2 1.2])
legend( 'f', 'S_N^\sigma[f]' );


% Compute the coefficients of the jump responses (Trigonometric, polynomial,
% exponential)
sigExp = fHat.*( 1i*sign( fourModes ).*sigProb3 ); 

% The jump responses
jmpRespExp = real( fourKern*sigExp );

% Plot
figure; plot(phyGrid, fx, 'k--'); hold on
plot( phyGrid, jmpRespExp, 'k' );
xlabel x; ylabel 'S_N[f](x)'
xlim([-pi pi]); ylim([-1.2 1.2])
legend( 'f', 'S_N^\sigma[f]' );
