clear all; close all; clc

% Alter figure label and text font sizes
set(0,'defaultaxesfontsize',15);
set(0,'defaulttextfontsize',15);


N = 128;                    % No. of modes to plot is 2N+1
fourModes = (-N:N).';       % The Fourier modes

% Compute the concentration factors (Trigonometric, polynomial,
% exponential)
sigTrig = confac( fourModes, 'Trig' );
sigPoly = confac( fourModes, 'Poly' );
sigExp = confac( fourModes, 'Exp' );

% Plot
figure; plot( fourModes, sigTrig, 'k'); hold on
plot( fourModes, sigPoly, 'k--');
plot( fourModes, sigExp, 'k-.');
xlabel k; ylabel \sigma(|k|/N);
title 'Concentration factors plotted in Fourier space'
legend( 'Trigonometric', 'Polynomial', 'Exponential' );
