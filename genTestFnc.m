function [fHat fx] = genTestFnc(fourModes, testFncType, phyGrid)

%% Test Function Generation
% Script to generate a test function in physical space, as well as its
% Fourier coefficients
%
% Usage:    [fHat, fx] = genTestFnc(fourModes, testFncType, phyGrid)
%
%   Inputs:
%       fourModes - the modes on which to compute the Fourier coefficients
%                   (real vector)
%       testFncType - the type of test function to generate (string)
%                     ( 'Dennis', 'Wolfgang', ... )
%       phyGrid - the physical space grid on which to compute the test
%                 function (real vector)
%
%   Outputs:
%       fHat - the Fourier coefficients (complex vector)
%       fx - the physical space function (real vector)

switch testFncType
    
    % If "the usual" (Dennis') test function is required
    case ( 'Dennis' )
        % Fourier modes computed analytically using Mathematica
        fHat = exp(3*pi*1i*fourModes/4).*(3./(4*pi*1i*fourModes)) + ...
            exp(1i*pi*fourModes/2).*(-3./(4*pi*1i*fourModes)) + ...
            exp(-3*1i*pi*fourModes/4).*(5./(1i*fourModes) - ...
            (33*pi)./(16*1i*fourModes) -11./(4*1i*1i*fourModes.*...
            fourModes))/(2*pi) + exp(-3*1i*pi*fourModes/8).*...
            (-5./(1i*fourModes) + (33*pi)./(32*1i*fourModes) + ...
            11./(4*1i*1i*fourModes.*fourModes))/(2*pi) + ...
            exp(-1i*pi*fourModes/8).*(-7./(4*1i*fourModes) + ...
            pi./(16*1i*fourModes) + 1./(2*1i*1i*fourModes.*fourModes) ...
            - sin(pi/8 - 1/4)*(fourModes./(1i*(fourModes.*fourModes-1)))...
            + cos(pi/8 - 1/4)*(1./(fourModes.*fourModes-1)) )/(2*pi) + ...
            exp(1i*pi*fourModes/4).*(7./(4*1i*fourModes) + ...
            pi./(8*1i*fourModes) - 1./(2*1i*1i*fourModes.*fourModes) +...
            sin(-pi/4 - 1/4)*(fourModes./(1i*(fourModes.*fourModes-1))) ...
            - cos(-pi/4 -1/4)*(1./(fourModes.*fourModes-1)) )/(2*pi);

        % The DC component
        if( sum(fourModes==0) == 1 )
            fHat(fourModes==0) = ( (-pi/2+3*pi/4)*1.5 - ...
                5*(3*pi/4-3*pi/8) + 11*(9*pi*pi/16 - 9*pi*pi/64)/8 ...
                + 7*(pi/8+pi/4)/4 - .25*(pi*pi/64 - pi*pi/16) - ...
                cos(pi/8 - .25) + cos(-pi/4 -.25) )/(2*pi);
        end
        
        % Components at k=+/-1
        if( sum(fourModes==1) == 1 )
            fHat(fourModes==1) = -( 48 - 160*(-1)^(1/8) - (56+16i)*...
                (-1)^(3/8) -88*(-1)^(5/8) + (100-60i)*sqrt(2) - 8*...
                sqrt( 28-45i ) + (4-4i)*((1-1i)+sqrt(2))*exp(1i/4) + ...
                (33*(-1)^(1/8)+2*(-1)^(3/8)-(35-35i)*sqrt(2))*pi + ...
                6i*pi*exp(-1i/4) )/(64*pi);
        end
        if( sum(fourModes==-1) == 1 )
            fHat(fourModes==-1) = exp(-1i/4)*( (-4-4i)*((1+1i)+sqrt(2)) ...
                + 6i*exp(1i/2)*pi + exp(1i/4)*(-8*(6+(2+7i)*(-1)^(1/8) +...
                11*(-1)^(3/8) + 20*(-1)^(7/8) + (8+5i)*sqrt(2)) + ( 2*...
                (-1)^(5/8) +33*(-1)^(7/8) +(35+35i)*sqrt(2))*pi) )/(64*pi);
        end        
        
        % And now, the physical space function
        fx = 1.5*(phyGrid>=-3*pi/4).*(phyGrid<-pi/2) + ...
                ( 7/4 - phyGrid/2 + sin(phyGrid-1/4) ).*(phyGrid>=-pi/4)...
                .*(phyGrid<pi/8) + ( phyGrid*11/4 - 5 ).*...
                (phyGrid>=3*pi/8).*(phyGrid<3*pi/4);


    % WOlfgang's take on Dennis' test function
    case ( 'Wolfgang' )
        % Fourier modes computed analytically using Mathematica
        fHat = ( ( -1+exp(-pi*1i*(fourModes-1i)/4) )./(-1-fourModes*1i) ...
            + 1i*( exp(5*1i*fourModes/2) - exp(1i*pi*fourModes/4) )./...
            fourModes + ( exp(-11*1i*fourModes/4).*...
            ( cos(55/4)*1i*fourModes - 5*sin(55/4) + ...
            exp(3*1i*fourModes/2).*( -1i*cos(25/4)*fourModes + ...
            5*sin(25/4) ) ) )./(2*(-25+fourModes.^2)) + 2*sin(3*...
            fourModes/4).*exp(-2*1i*fourModes)./fourModes )/(2*pi);

        % The DC component
        if( sum(fourModes==0) == 1 )
            fHat(fourModes==0) = ( -1.5 + pi/4 - cosh(pi/4) + ...
                sinh(pi/4) + (15-sin(25/4)+sin(55/4))/10 )/(2*pi);
        end

        % Components at k=+/-5
        if( sum(fourModes==5) == 1 )
            fHat(fourModes==5) = ( 3/8 - (1/26 - 5i/26)*(-1+(-1)^...
                (.75+1i/4))  + ((-1)^.75 + 1i*exp(25i/2))/5 + ...
            2*(sin(15/4)*exp(-10i))/5 + sin(15/2)*exp(-20i)/20 )/(2*pi);
        end
        if( sum(fourModes==-5) == 1 )
            fHat(fourModes==-5) = ( (1/26 + 5i/26)*(1+(-1)^...
                (.25+1i/4)) + (-(-1)^.25 - 1i*exp(-25i/2))/5 + ...
            (2*sin(15/2)*exp(20i) + 16*sin(15/4)*exp(10i) +15)/40 )/(2*pi);
        end
                
        % And now, the physical space function
        fx = -1*(phyGrid>=-2.5).*(phyGrid<-pi/4) + ...
                exp(-phyGrid).*(phyGrid>=0)...
                .*(phyGrid<pi/4) + ( 1 + 0.5*cos(5*phyGrid) ).*...
                (phyGrid>=1.25).*(phyGrid<2.75);

            
    % A function with some variation
    case ( 'Variation' )
        % Fourier modes computed analytically using Mathematica
        fHat = ( 6*(1+exp(5i*fourModes*pi/6))./(-36+fourModes.^2) + ...
                    (1-exp((-2-1i*fourModes)*pi)+(-2-1i*fourModes)*pi)...
                                    ./(pi*(fourModes-2i).^2) )/(2*pi);

        % Components at k=+/-6
        if( sum(fourModes==6) == 1 )
            fHat(fourModes==6) = ( 1/300 + 1i/400 )*( -(6+18i)*pi-...
                (30+40i)*pi^2 )/(pi^2);
        end
        if( sum(fourModes==-6) == 1 )
            fHat(fourModes==-6) = ( (30+90i)*pi + 250i*pi^2 )/(1200*pi^2);
        end
                
        % And now, the physical space function
        fx = sin(6*phyGrid).*(phyGrid>=-5*pi/6).*(phyGrid<0) + ...
                exp(-2*phyGrid).*((pi-phyGrid)/pi).*(phyGrid>=0);

            
    % A function with some variation
    case ( 'SinCosLin' )
        % Fourier modes computed analytically using Mathematica
        fHat = ( ((-1).^fourModes-1i*fourModes.*exp(1i*pi*fourModes/2))...
            ./(-1+fourModes.^2) + (((-1).^fourModes).*...
            (-4+exp(3i*fourModes*pi/4).*(4-3i*pi*fourModes)))./(4*fourModes.^2) + ...
            (exp(-1i*fourModes*pi/4).*(sqrt(2)*exp(3i*fourModes*pi/4).*...
            (3-2i*fourModes) + 6*cos(pi/8) - 4i*fourModes*sin(pi/8)...
            ))./(-9+4*fourModes.^2) )/(2*pi);

        % The DC component
        if( sum(fourModes==0) == 1 )
            fHat(fourModes==0) = ( 27*pi^2 - 32*(3+sqrt(2)+2*cos(pi/8)) )...
                /(192*pi);
        end                                
                                
        % Components at k=+/-1
        if( sum(fourModes==1) == 1 )
            fHat(fourModes==1) = ( 30+20*(-1)^(5/8) + 4*(-1)^(7/8) + ...
                (2-22i)*sqrt(2) - 5*pi*(1i+3*(-1)^(1/4)) )/(40*pi);
        end
        if( sum(fourModes==-1) == 1 )
            fHat(fourModes==-1) = ( 60-8*(-1)^(1/8) - 40*(-1)^(3/8) + ...
                (4+44i)*sqrt(2) + (5+5i)*pi*(1+1i+3i*sqrt(2)) )/(80*pi);
        end
                
        % And now, the physical space function
        fx = sin(phyGrid).*(phyGrid>=-pi).*(phyGrid<-pi/2) - ...
                cos(3*phyGrid/2).*(phyGrid>=-pi/2).*(phyGrid<pi/4) + ...
                    (pi-phyGrid).*(phyGrid>=pi/4).*(phyGrid<=pi);
            
end    
