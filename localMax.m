function [outVec locs] = localMax( inVec )

locs = [];
for idx = 2:round( length(inVec)/2 )
    if( inVec(idx) < inVec(idx-1) )
        locs = [locs idx];
    end
end

for idx = round( length(inVec)/2 ):length(inVec)-1
    if( inVec(idx) < inVec(idx+1) )
        locs = [locs idx];
    end
end

outVec = inVec;
outVec( locs ) = [];

return
