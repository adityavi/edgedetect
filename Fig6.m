clear all; close all; clc

% Alter figure label and text font sizes
set(0,'defaultaxesfontsize',15);
set(0,'defaulttextfontsize',15);


N = 64;                     % No. of modes to plot is 2N+1
fourModes = (-N:N).';       % The Fourier modes

% Physical grid
nPts = 256;
h = 2*pi/nPts;
phyGrid = -pi + h*(0:nPts-1).';

jmpKern = exp( 1i*phyGrid*fourModes(fourModes~=0).' )/(2*pi);

% CVX problem
cvx_begin
    variable sig(2*N+1);
    
    jmpRespCfs = sig(fourModes~=0)./abs( fourModes(fourModes~=0) );
    jmpResp = real( jmpKern*jmpRespCfs );
    minimize norm( jmpResp, 1 )
        subject to
            jmpResp(phyGrid==0) == 1;
cvx_end

figure; plot( fourModes, sig, 'k--');
xlabel k; ylabel '\sigma(|k|/N)'; xlim([-80 80])

% The unit ramp
ramp = ( (-phyGrid-pi).*(phyGrid<=0) + (-phyGrid+pi).*(phyGrid>0) )/(2*pi);

% Jump response plotted on a fine grid
nPts = 600;
h = 2*pi/nPts;
fineGrid = -pi + h*(0:nPts-1).';
jmpKern = exp( 1i*fineGrid*fourModes(fourModes~=0).' )/(2*pi);
jmpResp = real( jmpKern*jmpRespCfs );


figure; plot(phyGrid, ramp, 'k--'); hold on
plot( fineGrid, jmpResp, 'k');
xlabel x; ylabel 'W_0^\sigma^,^N(x)'; xlim([-pi pi]); ylim([-.75 1.25])
legend( 'f', 'S_N^\sigma[f]' );

save concFacProbForm2.mat sig